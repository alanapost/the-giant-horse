# The Giant Horse

Shared resources relating to the administration of that giant horse we all ride around on.

## Mastodon
### Admins
* [@alana@the.giant.horse](https://the.giant.horse/@alana) (backup: [@alanapost@mastodon.social](https://mastodon.social/@alanapost))
* [@chead@the.giant.horse](https://the.giant.horse/@chead)
* [@mitten@the.giant.horse](https://the.giant.horse/@mitten)

### Code of conduct
link to doc here

### Blocklist
link to doc here