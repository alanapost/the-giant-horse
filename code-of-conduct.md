## Code of Conduct
The Giant Horse is an invitation-only, general-interest instance hosted by [Masto.host - Fully Managed Mastodon Hosting](https://masto.host/)). Its monthly hosting fee is paid for privately; it is free to use.

When you receive and accept an invitation to join, you are indicating that you want to participate in our community. Here are some do’s and don’ts. If we are not a good match, or you want to register additional accounts, https://instances.social/ is a good resource.

### Do
* Use content warnings liberally. Think of them as email subject lines, if it helps!
* Provide written descriptions of visual media, so that we do not exclude people who use screen readers.
* Respond graciously to feedback or critique given in good faith. When people who are at the “margins” of a group (such as LGBTQ folks, people with disabilities, poor and working-class people) have feedback or choose to speak, they don’t need to be "polite" or avoid tension. Please take a break to reduce your distress if you are struggling with this. Diaphragmatic breathing exercises can have a quick and positive impact on your emotional state.
* Respect confidentiality.
* Presume goodwill on others’ behalves.
* If you are able, call “in” rather than “out”.
	* Recognize we all are learning, and speak from this shared experience.
	* Be specific and direct.
	* Talk to people in times and places that support conversation, such as via direct message instead of a public dragging.

### Do Not
* Do not archive (e.g. take screenshots of) other people’s posts. You will not need “receipts” here. If there is a disagreement, we will resolve it in a privacy- and safety-respecting way.
* If you are blocked by someone, accept it and move on. Do not attempt to evade a block, or ask anyone to help you evade a block.
* Do not begin nor participate in harassment campaigns.
* Do not register sock puppet accounts.
* Do not post any content that is racist, sexist, transphobic, xenophobic, or antagonistic toward poor and working-class people, people with disabilities, LGBTQ people, the elderly, or children.
* Do not spread false or intentionally misleading information.

### What If I Have a Code-Related Question or Concern?
…
### Reporting
…
